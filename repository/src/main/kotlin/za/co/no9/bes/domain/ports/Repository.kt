package za.co.no9.bes.domain.ports

import java.util.*


interface Repository {
    fun newUnitOfWork(): UnitOfWork
}


typealias EventID =
        String

typealias TopicID =
        Int


interface UnitOfWork {
    fun addEvent(eventName: String, version: Int, topicID: TopicID, content: String): EventDTO

    fun event(id: EventID): EventDTO?

    fun events(from: EventID? = null, pageSize: Int = 100, topicID: TopicID? = null, action: (EventDTO) -> Unit)

    fun events(from: EventID? = null, pageSize: Int = 100, topicID: TopicID? = null): Sequence<EventDTO> {
        val result =
                mutableListOf<EventDTO>()

        events(from, pageSize, topicID) { result.add(it) }

        return result.asSequence()
    }
}


data class EventDTO(
        val id: EventID,
        val `when`: Date,
        val type: EventTypeDTO,
        val topicID: TopicID,
        val content: String)

data class EventTypeDTO(
        val name: String,
        val version: Int)
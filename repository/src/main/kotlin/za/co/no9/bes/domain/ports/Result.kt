package za.co.no9.bes.domain.ports

sealed class Result<out E, out V> {
    data class Error<out E>(val error: E) : Result<E, Nothing>()
    data class Value<out V>(val value: V) : Result<Nothing, V>()
}


fun <V> value(value: V): Result<Nothing, V> =
        Result.Value(value)

fun <E> error(value: E): Result<E, Nothing> =
        Result.Error(value)


fun <V> either(action: () -> V): Result<Exception, V> =
        try {
            value(action())
        } catch (e: Exception) {
            error(e)
        }


inline infix fun <E, V, V2> Result<E, V>.map(f: (V) -> V2): Result<E, V2> =
        when (this) {
            is Result.Error ->
                this

            is Result.Value ->
                Result.Value(f(this.value))
        }


inline infix fun <E, V, V2> Result<E, V>.andThen(f: (V) -> Result<E, V2>): Result<E, V2> =
        when (this) {
            is Result.Error ->
                this

            is Result.Value ->
                f(this.value)
        }


infix fun <E, V, V2> Result<E, (V) -> V2>.apply(f: Result<E, V>): Result<E, V2> =
        when (this) {
            is Result.Error ->
                this

            is Result.Value ->
                f.map(this.value)
        }


inline infix fun <E, V, V2> Result<E, V>.flatMap(f: (V) -> Result<E, V2>): Result<E, V2> =
        when (this) {
            is Result.Error ->
                this

            is Result.Value ->
                f(value)
        }


inline infix fun <E, E2, V> Result<E, V>.mapError(f: (E) -> E2): Result<E2, V> =
        when (this) {
            is Result.Error ->
                Result.Error(f(error))

            is Result.Value ->
                this
        }


inline fun <E, V, A> Result<E, V>.fold(e: (E) -> A, v: (V) -> A): A =
        when (this) {
            is Result.Error ->
                e(this.error)

            is Result.Value ->
                v(this.value)
        }


fun <E, V> Result<E, V>.left(): E? =
        when (this) {
            is Result.Error ->
                this.error

            is Result.Value ->
                null
        }


fun <E, V> Result<E, V>.right(): V? =
        when (this) {
            is Result.Error ->
                null

            is Result.Value ->
                this.value
        }

package za.co.no9.bes.domain.ports

import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec


class ResultTest : BehaviorSpec({
    Given("error(10)") {
        val value =
                error(10)

        When("mapError{ it * 10}") {
            val result =
                    value.mapError { it * 10 }

            Then("it should be 100") {
                result shouldBe error(100)
            }
        }
    }

    Given("value(10)") {
        val value =
                value(10)

        When("map{it * 10}") {
            val result =
                    value.map { it * 10 }

            Then("it should be 100") {
                result shouldBe value(100)
            }
        }
    }
})

package za.co.no9.bes.domain.ports.repository

import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec
import za.co.no9.bes.domain.ports.Repository
import za.co.no9.bes.domain.ports.TopicID
import za.co.no9.bes.domain.ports.UnitOfWork
import za.co.no9.bes.domain.ports.right


abstract class BasicStreamTestParent : BehaviorSpec() {
    init {
        val UNKNOWN_TOPIC_ID =
                -1

        Given("An empty event store") {
            emptyEventStore()

            When("invoking events()") {
                val eventDTOs =
                        unitOfWork().events()

                Then("returns an empty stream") {
                    eventDTOs.count() shouldBe 0
                }
            }

            When("invoking events(pageSize = 10)") {
                val eventDTOs =
                        unitOfWork().events(pageSize = 10)

                Then("returns an empty stream") {
                    eventDTOs.count() shouldBe 0
                }
            }

            When("invoking events(from = \"hello\", pageSize = 10)") {
                val eventDTOs =
                        unitOfWork().events(from = "hello", pageSize = 10)

                Then("returns an empty stream") {
                    eventDTOs.count() shouldBe 0
                }
            }

            When("invoking events(from = \"hello\", pageSize = 10, topicID = UNKNOWN_TOPIC_ID)") {
                val eventDTOs =
                        unitOfWork().events(from = "hello", pageSize = 10, topicID = UNKNOWN_TOPIC_ID)

                Then("returns an empty stream") {
                    eventDTOs.count() shouldBe 0
                }
            }
        }

        Given("A populated event store") {
            val VALID_TOPIC_ID =
                    populatedEventStore()

            When("invoking events()") {
                val eventDTOs =
                        unitOfWork().events().toList()

                Then("returns the first 100 events") {
                    eventDTOs.size shouldBe 100
                    eventDTOs.forEachIndexed { index, eventDTO -> eventDTO.type.version shouldBe index }
                }
            }

            When("invoking events(topic = VALID_TOPIC_ID)") {
                val eventDTOs =
                        unitOfWork().events(topicID = VALID_TOPIC_ID).toList()

                Then("returns the first 100 events") {
                    eventDTOs.size shouldBe 100
                    eventDTOs.forEachIndexed { index, eventDTO -> eventDTO.type.version shouldBe index }
                }
            }

            When("invoking events(topicID = UNKNOWN_TOPIC_ID)") {
                val eventDTOs =
                        unitOfWork().events(topicID = UNKNOWN_TOPIC_ID).toList()

                Then("returns an empty stream") {
                    eventDTOs.size shouldBe 0
                }
            }

            And("and the event ID of the 10th event") {
                val tenthEventId =
                        unitOfWork().events().toList()[10].id

                When("invoking events from the 10th event ID") {
                    val eventDTOs =
                            unitOfWork().events(from = tenthEventId)

                    Then("100 events are returned") {
                        eventDTOs.count() shouldBe 100
                    }

                    Then("the returned events are from the 10th event") {
                        eventDTOs.forEachIndexed { index, eventDTO -> eventDTO.type.version shouldBe (index + 11) }
                    }
                }
            }

            When("invoking events(pageSize = 10") {
                val eventDTOs =
                        unitOfWork().events(pageSize = 10)

                Then("10 events are returned") {
                    eventDTOs.count() shouldBe 10
                }

                Then("the returned events are the first 10") {
                    eventDTOs.forEachIndexed { index, eventDTO -> eventDTO.type.version shouldBe index }
                }
            }

            When("invoking events(pageSize = 10, topicID = UNKNOWN_TOPIC_ID)") {
                val eventDTOs =
                        unitOfWork().events(pageSize = 10, topicID = UNKNOWN_TOPIC_ID)

                Then("no events are returned") {
                    eventDTOs.count() shouldBe 0
                }
            }

            When("invoking events(pageSize = 10, topicID = VALID_TOPIC_ID") {
                val eventDTOs =
                        unitOfWork().events(pageSize = 10, topicID = VALID_TOPIC_ID)

                Then("10 events are returned") {
                    eventDTOs.count() shouldBe 10
                }

                Then("the returned events are the first 10") {
                    eventDTOs.forEachIndexed { index, eventDTO -> eventDTO.type.version shouldBe index }
                }
            }

            When("invoking events(from = \"hello\", pageSize = 10)") {
                val eventDTOs =
                        unitOfWork().events(from = "hello", pageSize = 10)

                Then("10 events are returned") {
                    eventDTOs.count() shouldBe 10
                }

                Then("the returned events are the first 10") {
                    eventDTOs.forEachIndexed { index, eventDTO -> eventDTO.type.version shouldBe index }
                }
            }

            When("invoking events(from = \"hello\", pageSize = 10, topicID = UNKNOWN_TOPIC_ID)") {
                val eventDTOs =
                        unitOfWork().events(from = "hello", pageSize = 10, topicID = UNKNOWN_TOPIC_ID)

                Then("no events are returned") {
                    eventDTOs.count() shouldBe 0
                }
            }

            When("invoking events(from = \"hello\", pageSize = 10, topicID = VALID_TOPIC_ID)") {
                val eventDTOs =
                        unitOfWork().events(from = "hello", pageSize = 10, topicID = VALID_TOPIC_ID)

                Then("10 events are returned") {
                    eventDTOs.count() shouldBe 10
                }

                Then("the returned events are the first 10") {
                    eventDTOs.forEachIndexed { index, eventDTO -> eventDTO.type.version shouldBe index }
                }
            }
        }

        Given("An event store") {
            emptyEventStore()

            When("attempt to get an event using an invalid event ID") {
                val event =
                        unitOfWork().event("hello")

                Then("nothing is returned") {
                    event shouldBe null
                }
            }

            When("attempt to get an event using an unknown event ID") {
                val event =
                        unitOfWork().event("-1")

                Then("nothing is returned") {
                    event shouldBe null
                }
            }
        }
    }


    private fun populatedEventStore(): TopicID {
        this.emptyEventStore()

        val unitOfWork =
                unitOfWork()

        for (lp in 0..200) {
            unitOfWork.addEvent("TestEvent", lp, 1, "Content")
        }

        return 1
    }


    abstract fun unitOfWork(): UnitOfWork

    abstract fun emptyEventStore()
}


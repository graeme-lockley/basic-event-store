package za.co.no9.bes.domain.ports

import za.co.no9.bes.domain.ports.repository.BasicStreamTestParent
import za.co.no9.bes.domain.ports.repository.InMemory


val inMemory =
        InMemory()


class BasicStreamTest : BasicStreamTestParent() {
    override fun unitOfWork() =
            inMemory.newUnitOfWork()

    override fun emptyEventStore() {
        inMemory.truncateStore()
    }
}

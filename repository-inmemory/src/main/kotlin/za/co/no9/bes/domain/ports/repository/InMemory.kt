package za.co.no9.bes.domain.ports.repository

import za.co.no9.bes.domain.ports.*
import java.time.Instant
import java.util.*


class InMemory : Repository, UnitOfWork {
    private val savedEvents =
            mutableListOf<EventDTO>()

    private var idCounter =
            0


    fun truncateStore() {
        savedEvents.clear()
        idCounter = 0
    }


    override fun newUnitOfWork(): UnitOfWork =
            this


    override fun addEvent(eventName: String, version: Int, topicID: TopicID, content: String): EventDTO {
        val eventDTO =
                EventDTO(idCounter.toString(), Date.from(Instant.now()), EventTypeDTO(eventName, version), topicID, content)

        savedEvents.add(eventDTO)
        idCounter += 1

        return eventDTO
    }


    override fun event(id: EventID): EventDTO? =
            savedEvents.firstOrNull { event -> event.id == id }


    override fun events(from: EventID?, pageSize: Int, topicID: TopicID?, action: (EventDTO) -> Unit) {
        val stream =
                when (val fromID = from?.toLongOrNull()) {
                    null ->
                        savedEvents

                    else ->
                        savedEvents.dropWhile { it.id.toLong() <= fromID }
                }

        when (topicID) {
            null ->
                stream

            else ->
                stream.filter { it.topicID == topicID }
        }.take(pageSize).forEach(action)
    }
}

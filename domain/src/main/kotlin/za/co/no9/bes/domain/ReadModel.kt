package za.co.no9.bes.domain

import com.google.gson.Gson
import mu.KotlinLogging
import za.co.no9.bes.domain.ports.EventID
import za.co.no9.bes.domain.ports.Repository
import za.co.no9.bes.domain.ports.TopicID


val gson =
        Gson()


class ReadModel(private val repository: Repository, private var eventID: EventID? = null, var state: State = State()) {
    private val logger =
            KotlinLogging.logger {}


    fun processEvents(): ReadModel {
        repository.newUnitOfWork().events(eventID, Int.MAX_VALUE, CONFIG_TOPIC_ID) {
            eventID = it.id

            try {
                state = state.processEvent(unmarshall(it.content))
            } catch (e: Exception) {
                logger.error(e) { "Attempt to unmarshall EventDTO: $it" }
            }
        }

        return this
    }


    fun topic(id: Int): Topic? =
            state.topic(id)

    fun topic(code: String): Topic? =
            state.topic(code)


    fun topics(from: TopicID?, pageSize: Int): Sequence<Topic> =
            state.topics(from, pageSize)
}


class State(val topics: List<Topic> = listOf(Topic(CONFIG_TOPIC_ID, CONFIG_TOPIC_CODE, "Store configuration events"))) {
    fun processEvent(topicAdded: TopicAdded): State {
        val topicID =
                1 + (topics.map { it.id }.max() ?: 0)

        return State(this.topics + Topic(topicID, topicAdded.code, topicAdded.name))
    }


    fun topic(id: Int): Topic? =
            topics.firstOrNull { it.id == id }

    fun topic(code: String): Topic? =
            topics.firstOrNull { it.code == code }


    fun topics(from: TopicID?, pageSize: Int): Sequence<Topic> =
            when (from) {
                null ->
                    topics

                else ->
                    topics.dropWhile { it.id.toLong() <= from }
            }.take(pageSize).asSequence()
}


fun unmarshall(text: String): TopicAdded {
    val result =
            gson.fromJson(text, TopicAdded::class.java)

    when {
        result.code == null ->
            throw NullPointerException("code")

        result.name == null ->
            throw NullPointerException("name")

        else ->
            return result
    }
}


fun marshall(topicAdded: TopicAdded): String =
        gson.toJson(topicAdded)

package za.co.no9.bes.domain

import za.co.no9.bes.domain.ports.*


val CONFIG_TOPIC_ID =
        0

val CONFIG_TOPIC_CODE =
        "CONFIG"


class Services(private val repository: Repository) {
    private val readModel =
            ReadModel(repository)


    fun newTopic(code: String, name: String): Result<DuplicateCode, Topic> =
            if (topic(code) == null) {
                addEvent("TopicAdded", 1, 0, marshall(TopicAdded(code, name)))

                value(topic(code)!!)
            } else {
                error(DuplicateCode(code))
            }


    fun topic(id: TopicID): Topic? =
            readModel.processEvents().topic(id)

    fun topic(code: String): Topic? =
            readModel.processEvents().topic(code)


    fun topics(from: TopicID? = null, pageSize: Int = 100): Sequence<Topic> =
            readModel.processEvents().topics(from, pageSize)


    fun addEvent(name: String, version: Int, topicID: TopicID, content: String): Event =
            repository
                    .newUnitOfWork()
                    .addEvent(name, version, topicID, content)
                    .toEvent(this)


    fun events(from: String? = null, pageSize: Int = 100): Sequence<Event> =
            repository.newUnitOfWork().events(from, pageSize).map { it.toEvent(this) }
}


open class Errors

data class DuplicateCode(val code: String) : Errors()


private fun EventDTO.toEvent(services: Services) =
        Event(services, this.id, this.`when`, this.type, this.topicID, this.content)


data class TopicAdded(val code: String, val name: String)
package za.co.no9.bes.domain

import za.co.no9.bes.domain.ports.EventID
import za.co.no9.bes.domain.ports.EventTypeDTO
import za.co.no9.bes.domain.ports.TopicID
import java.util.*


class Event(private val services: Services, val id: EventID, val `when`: Date, val type: EventTypeDTO, private val topicID: TopicID, val content: String) {
    val topic: Topic?
        get() = services.topic(topicID)
}
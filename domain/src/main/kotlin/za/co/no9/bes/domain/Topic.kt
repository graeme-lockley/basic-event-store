package za.co.no9.bes.domain


class Topic(val id: Int, val code: String, val name: String)
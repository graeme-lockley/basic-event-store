package za.co.no9.bes.domain

import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.BehaviorSpec
import za.co.no9.bes.domain.ports.EventTypeDTO
import za.co.no9.bes.domain.ports.left
import za.co.no9.bes.domain.ports.repository.InMemory
import za.co.no9.bes.domain.ports.right


val DEFAULT_TOPIC_CODE =
        "DEFAULT"


class TopicsScenarioTest : BehaviorSpec({

    Given("An event store") {
        val services =
                emptyEventStore()

        When("a new unique topic is created") {
            val topicResult =
                    services.newTopic("TOPIC", "My test topic")

            Then("no error is reported") {
                topicResult.right() shouldNotBe null
            }

            Then("that topic is available through topics") {
                val topic =
                        topicResult.right()!!

                val topics =
                        services.topics(pageSize = Int.MAX_VALUE)

                topics.find { it.id == topic.id } shouldBe topic
            }

            Then("that topic can be accessed through topic") {
                val topic =
                        topicResult.right()!!

                val retrievedTopic =
                        services.topic(topic.id)!!

                retrievedTopic shouldNotBe null
                retrievedTopic.id shouldBe topic.id
                retrievedTopic.code shouldBe topic.code
                retrievedTopic.name shouldBe topic.name
            }
        }
    }

    Given("An event store with a registered topic") {
        val services =
                emptyEventStore()

        services.newTopic("TOPIC", "My test topic")

        When("an attempt is made to create a duplicate topic") {
            val topicResult =
                    services.newTopic("TOPIC", "My test topic")

            Then("an error is reported") {
                topicResult.left() shouldNotBe null
            }

            Then("the error is DuplicateCode") {
                topicResult.left() shouldBe DuplicateCode("TOPIC")
            }
        }
    }

    Given("An event store with many topics") {
        val services =
                eventStoreWithTopics()

        When("invoke topics(from = null, pageSize = 10)") {
            val topics =
                    services.topics(from = null, pageSize = 10)

            Then("10 topics are returned") {
                topics.count() shouldBe 10
            }

            Then("the result is the first 10 topics") {
                val keyTopics =
                        topics.filter { it.code != CONFIG_TOPIC_CODE }

                keyTopics.forEachIndexed { index, topic -> topic.code shouldBe "KEY$index" }
            }
        }

        When("invoke topics(from = 99, pageSize = 10)") {
            val topics =
                    services.topics(from = 99, pageSize = 10)

            Then("10 topics are returned") {
                topics.count() shouldBe 10
            }

            Then("the result is the first 10 topics following the topic with id = 99") {
                val keyTopics =
                        topics.filter { it.code != CONFIG_TOPIC_CODE }

                keyTopics.forEachIndexed { index, topic -> topic.code shouldBe "KEY${index + 99}" }
            }
        }
    }
})


class EventsScenarioTest : BehaviorSpec({
    Given("An empty event store") {
        val services =
                emptyEventStore()

        When("invoke events()") {
            val events =
                    services.events()

            Then("an empty stream of events is returned") {
                events.count() shouldBe 0
            }
        }
    }

    Given("An event store containing events") {
        val services =
                eventStoreWithEvents()


        When("invoke events(pageSize = 100)") {
            val events =
                    services.events(pageSize = 100)

            Then("will return 100 events") {
                events.count() shouldBe 100
            }

            Then("each event in the DEFAULT topic will be correctly populated") {
                val defaultEvents =
                        events.filter { it.topic?.code == DEFAULT_TOPIC_CODE }

                defaultEvents.all { it.type == EventTypeDTO("EventName", 1) } shouldBe true
                defaultEvents.all { it.topic!!.code == "DEFAULT" } shouldBe true
                defaultEvents.all { it.content == "HelloWorld" } shouldBe true
            }
        }

        And("the 10th event ID") {
            val eventID =
                    services.events().toList()[9].id

            When("invoking events(from = 10thID, pageSize = 10)") {
                val events =
                        services.events(from = eventID, pageSize = 10)

                And("events(pageSize = 20).drop(10)") {
                    val otherEvents =
                            services.events(pageSize = 20).drop(10)

                    Then("they should return the same sequence of event IDs") {
                        otherEvents.map { it.id }.toList() shouldBe events.map { it.id }.toList()
                    }
                }
            }
        }

        When("I attempt to add an event with a valid topic ID") {
            val validTopicID =
                    services.newTopic("NewTopic", "New Topic").right()!!.id

            val eventResult =
                    services.addEvent("NewEvent", 2, validTopicID, "Content")

            Then("the event should be endured") {
                eventResult.content shouldBe "Content"
                eventResult.topic!!.id shouldBe validTopicID
                eventResult.type.name shouldBe "NewEvent"
                eventResult.type.version shouldBe 2
            }
        }
    }
})


fun emptyEventStore(): Services =
        Services(InMemory())


fun eventStoreWithTopics(): Services {
    val repository =
            InMemory()

    val services =
            Services(repository)

    for (lp in 0..200) {
        services.newTopic("KEY$lp", "Key $lp")
    }

    return services
}


fun eventStoreWithEvents(): Services {
    val repository =
            InMemory()

    val services =
            Services(repository)

    val topic =
            services.newTopic(DEFAULT_TOPIC_CODE, "Default Topic").right()!!

    for (lp in 0..200) {
        repository.newUnitOfWork().addEvent("EventName", 1, topic.id, "HelloWorld")
    }

    return services
}
package za.co.no9.bes.domain

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec


class StateTest : StringSpec({
    "new State has a single CONFIG topic" {
        val topics =
                State().topics

        topics.size shouldBe 1

        topics[0].id shouldBe 0
        topics[0].code shouldBe "CONFIG"
    }


    "processing TopicAdded event should result in a new topic being appended with a unique ID" {
        val initState =
                State()

        val newState =
                initState.processEvent(TopicAdded("Test", "Test Topic"))


        newState.topics.size shouldBe initState.topics.size + 1

        newState.topics[newState.topics.lastIndex].id shouldBe 1
        newState.topics[newState.topics.lastIndex].code shouldBe "Test"

        newState.topics.map { it.id }.toSet().size shouldBe newState.topics.size
    }
})
package za.co.no9.bes.domain

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import za.co.no9.bes.domain.ports.repository.InMemory


class ReadModelTest : StringSpec({
    "processing over an empty repository leaves the state in tact" {
        val readModel =
                ReadModel(InMemory())

        val initState =
                readModel.state

        readModel.processEvents()

        val finalState =
                readModel.state

        finalState shouldBe initState
    }


    "processing 2 AddTopic events will result in the state being updated accordingly" {
        val repository =
                InMemory()

        val readModel =
                ReadModel(repository)

        repository.addEvent("TopicAdded", 1, 0, marshall(TopicAdded("Topic1", "Topic One")))
        repository.addEvent("TopicAdded", 1, 0, marshall(TopicAdded("Topic2", "Topic Two")))

        readModel.processEvents()

        readModel.state.topics.size shouldBe 3
    }


    "processing invalid event content over the CONFIG topic will result in the state left unchanged" {
        val repository =
                InMemory()

        val readModel =
                ReadModel(repository)

        repository.addEvent("TopicAdded", 1, 0, marshall(TopicAdded("Topic1", "Topic One")))
        repository.addEvent("TopicAdded", 1, 0, "{\"codes\":\"Topic2\",\"name\":\"Topic Two\"}")
        repository.addEvent("TopicAdded", 1, 0, marshall(TopicAdded("Topic3", "Topic Three")))

        readModel.processEvents()

        readModel.state.topics.size shouldBe 3
    }
})
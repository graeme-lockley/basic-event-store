package za.co.no9.bes.domain.ports.repository

import org.jdbi.v3.core.Jdbi
import za.co.no9.bes.domain.ports.*
import java.sql.Timestamp
import java.time.Instant
import java.util.*


class H2(private val jdbi: Jdbi) : Repository {
    fun truncateStore() {
        jdbi.withHandle<Unit, java.lang.RuntimeException> { handle ->
            handle.execute("set referential_integrity false")
            handle.execute("truncate table event")
            handle.execute("set referential_integrity true")
        }
    }

    override fun newUnitOfWork(): UnitOfWork =
            H2UnitOfWork(jdbi)
}


class H2UnitOfWork(private val jdbi: Jdbi) : UnitOfWork {
    override fun addEvent(eventName: String, version: Int, topicID: TopicID, content: String): EventDTO =
            jdbi.withHandle<EventDTO, RuntimeException> { handle ->
                handle.execute("insert into event (when, name, version, topicID, content) values (?, ?, ?, ?, ?)", Timestamp(Date.from(Instant.now()).time), eventName, version, topicID, content)

                val eventDTO =
                        handle.createQuery("select id, when, name, version, topicid, content from event where id = SCOPE_IDENTITY()")
                                .map { rs, _ -> EventDTO(rs.getLong("id").toString(), rs.getTimestamp("when"), EventTypeDTO(rs.getString("name"), rs.getInt("version")), rs.getInt("topicid"), rs.getString("content")) }
                                .one()

                eventDTO
            }


    override fun event(id: EventID): EventDTO? {
        val longID =
                id.toLongOrNull()

        return if (longID == null) {
            null
        } else {
            jdbi.withHandle<EventDTO?, RuntimeException> { handle ->
                handle.select("select id, when, name, version, topicid, content from event where id = ?", longID)
                        .map { rs, _ -> EventDTO(rs.getInt("id").toString(), rs.getTimestamp("when"), EventTypeDTO(rs.getString("name"), rs.getInt("version")), rs.getInt("topicid"), rs.getString("content")) }
                        .findFirst()
                        .orElse(null)
            }
        }
    }


    override fun events(from: EventID?, pageSize: Int, topicID: TopicID?, action: (EventDTO) -> Unit) {
        val fromID =
                from?.toLongOrNull()

        jdbi.withHandle<Unit, RuntimeException> { handle ->
            val query =
                    if (fromID == null) {
                        if (topicID == null)
                            handle.select("select id, when, name, version, topicid, content from event order by id limit ?", pageSize)
                        else
                            handle.select("select id, when, name, version, topicid, content from event where topicid = ? order by id limit ?", topicID, pageSize)
                    } else if (topicID == null)
                        handle.select("select id, when, name, version, topicid, content from event where id > ? order by id limit ?", fromID, pageSize)
                    else
                        handle.select("select id, when, name, version, topicid, content from event where id > ? and topicid = ? order by id limit ?", fromID, topicID, pageSize)

            query.map { rs, _ -> EventDTO(rs.getInt("id").toString(), rs.getTimestamp("when"), EventTypeDTO(rs.getString("name"), rs.getInt("version")), rs.getInt("topicid"), rs.getString("content")) }
                    .forEach(action)
        }
    }
}
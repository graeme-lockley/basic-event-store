package za.co.no9.bes.domain.ports

import liquibase.Liquibase
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import org.jdbi.v3.core.Jdbi
import za.co.no9.bes.domain.ports.repository.BasicStreamTestParent
import za.co.no9.bes.domain.ports.repository.H2
import java.sql.DriverManager


val h2 =
        H2(initialiseDatabase("jdbc:h2:mem:", "sa", ""))


class BasicStreamTest : BasicStreamTestParent() {
    override fun unitOfWork() =
            h2.newUnitOfWork()

    override fun emptyEventStore() {
        h2.truncateStore()
    }
}


private fun initialiseDatabase(url: String, username: String, password: String): Jdbi {
    val connection =
            DriverManager.getConnection(url, username, password)

    val database =
            liquibase.database.DatabaseFactory.getInstance().findCorrectDatabaseImplementation(JdbcConnection(connection))

    val liquibase =
            Liquibase("h2.schema.xml", ClassLoaderResourceAccessor(), database)

    liquibase.update("")

    return Jdbi.create(connection)
}
